Prérequis
comme prérequis pour le projet :

- Un serveur Web (Ex.: Wamp)
- PHP 8.2.0
- MySql 8.0.33
- Git
- Composer

// installation du projet
Ouvrez votre git bash, accéder à votre répertoire www de votre serveur en utilisant la commande cd

Cloner le projet à l'aide de la commande git clone le lien
gitLab

 Installation du Projet dans le www
Cloner le Projet :
    - Ouvrez Git Bash et accédez à votre répertoire www avec `cd`.
    - Clonez le projet en utilisant la commande suivante :
      ```bash
      git clone https://gitlab.com/Mariusfoko78/projet-laravel.git
      ```

2. **Installation de Laravel :**
    - Utilisez les commandes suivantes dans l'invite de commande dans cet ordre :
      ```bash
      composer install  # Installe les dépendances de Laravel
      php artisan installproject  # Installe le projet, crée le fichier .env
      php artisan db Migration  # Fait la migration des tables et insère les données des cours
      
      ```

3. **Création de la Base de Données :**
    - Allez sur PHPMyAdmin pour créer la base de données en utilisant le même nom que lors de l'installation.
    - Assurez-vous d'utiliser le format utf8mb4_general_ci.

...

Exécution de l'Application Laravel :

Si vous utilisez la commande php artisan serve pour exécuter votre application localement,
assurez-vous que le serveur Laravel est en cours d'exécution. Dans le terminal, utilisez la commande suivante :

php artisan serve

Cela lancera le serveur de développement Laravel, et vous pourrez accéder à votre projet via l'URL indiquée
(généralement http://127.0.0.1:8000/).

Nettoyage du Cache (si nécessaire) :

Si vous rencontrez des problèmes avec les modifications qui ne semblent pas s'afficher correctement,
vous pouvez essayer de nettoyer le cache de votre application avec la commande :
php artisan cache:clear
Et assurez-vous de rafraîchir votre navigateur.


