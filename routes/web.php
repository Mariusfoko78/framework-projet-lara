<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthManager;
use App\Http\Controllers\userController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\AdminController;



use App\Http\Controllers\CourseController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'home'])->name('home');
Route::get('/Liste-des-cours', [App\Http\Controllers\CourseController::class, 'course'])->name('course');

Route::get('/logout', [LoginController::class, 'logout'])->name('logout');



Route::get('/profil', [UserController::class, 'profil'])->name('profil');
Route::get('/Liste-des-utilisateurs',[userController::class,'user'])->name('Liste-des utilisateur');

// Route pour gérer les exports json
Route::get('/export-profile', [ProfilController::class, 'export'])->name('export');

Route::get('/profil', [ProfilController::class, 'show'])->name('profil');


// Route pour afficher le formulaire de connexion


Route::get('/login', [App\Http\Controllers\LoginController::class, 'login'])->name('login');
Route::post('/login', [LoginController::class, 'authenticate']);




Route::get('/profil/edit', [ProfilController::class, 'edit'])->name('profil.edit');
Route::post('/profil/update', [ProfilController::class, 'update'])->name('profil.update');


// Route pour l'admin

Route::middleware(['App\Http\Middleware\AdminMiddleware'])->group(function () {
    Route::get('admin', 'App\Http\Controllers\userController@admin')->name('admin');
});




Route::post('/login', [AuthManager::class, 'loginPost'])->name('login.post');
Route::put('/users/{id}', [UserController::class, 'update'])->name('users.update');

Route::get('/registration', [AuthManager::class, 'registration'])->name('registration');
Route::post('/registration', [AuthManager::class, 'registrationPost'])->name('registration.post');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');


// routes/web.php



Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/admin/users', [AdminController::class, 'usersList'])->name('admin.users.list');
});






