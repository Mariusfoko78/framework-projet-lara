@include('include.header')
@include('layout')
@section('content')
    @extends('layout') <!-- Assurez-vous d'étendre votre mise en page principale qui inclut Bootstrap -->

    @section('content')
        <div class="container">
            <h1>Liste des cours</h1>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Nom du cours</th>
                    <th>Code</th>
                </tr>
                </thead>
                <tbody>
                @foreach($courses as $course)
                    <tr>
                        <td>{{$course->name}}</td>
                        <td>{{$course->code}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endsection

