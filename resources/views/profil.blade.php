@include('include.header')

@section('content')
    @extends('layout')
    <div class="container">
        <table class="table table-striped">
            @if(session()->has('success'))
                <div class="alert alert-success">{{session('success')}}</div>
            @endif

            <h1>Profil </h1>
        <th>Intitulé</th>
        <th class="">Valeur</th>
            <tr>
                <th>Identifiant :</th>
                <td>{{ $user->username }}</td>
            </tr>
            <tr>
                <th>Email :</th>
                <td>{{ $user->email }}</td>
            </tr>
            <tr>
                <th>Création :</th>
                <td>{{ $user->created_at->format('d/m/Y H:i:s') }}</td>
            </tr>
            <tr>
                <th>Dernière visite :</th>
                <td>{{date_format(new DateTime($user->lastlogin),' d/m/y H:i:s')}}</td>
            </tr>
            <tr>
                <th>Rôle</th>
                @if($user->role == 1)
                    <td>Étudiant</td>
                @elseif($user->role == 2)
                    <td>Professeur</td>
                @elseif($user->role == 3)
                    <td>Administrateur</td>
                @else
                    <td>Visiteur</td>
                @endif
            </tr>
        </table>
        <a href="{{ route('export') }}" class="btn btn-primary">Exporter le profil en JSON</a>
        <div class="container">


            <h3><a href="#profile-update-collapse" data-bs-toggle="collapse" role="button" class="text-decoration-none ">Modifier le profil</a></h3>
            <form method="POST" action="{{ route('profil.update') }}">
                @csrf

                <div class="mb-4">
                    <label for="password" class="form-label">Nouveau mot de passe </label>
                    <input type="password" name="password" class="form-control">

                    @error('password')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>

                <div class="mb-3">
                    <label for="email" class="form-label">Nouvelle adresse email</label>
                    <input type="email" name="email" class="form-control" value="{{ old('email', $user->email) }}">

                </div>

                <button type="submit" class="btn btn-primary">Enregistrer les modifications</button>

            </form>

        </div>







    </div>



@endsection


