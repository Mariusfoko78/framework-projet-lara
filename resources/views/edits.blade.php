<!-- resources/views/profil/edits.blade.php -->




    <div class="container">

        <h2>Modifier le profil</h2>
        <form method="POST" action="{{ route('profil.update') }}">
            @csrf
            @method('PATCH')

            <div class="mb-3">
                <label for="password" class="form-label">Nouveau mot de passe </label>
                <input type="password" name="password" class="form-control">
            </div>

            <div class="mb-3">
                <label for="email" class="form-label">Nouvelle adresse email</label>
                <input type="email" name="email" class="form-control" value="{{ old('email', $user->email) }}">
            </div>

            <button type="submit" class="btn btn-primary">Enregistrer les modifications</button>
        </form>
        @if(session()->has('success'))
            <div class="alert alert-success">{{session('success')}}</div>
        @endif
    </div>
@endsection
