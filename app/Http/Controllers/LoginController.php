<?php

namespace App\Http\Controllers;


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login()
    {
        return view('login');
    }

    // Méthode pour gérer la soumission du formulaire de connexion
    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        if (Auth::attempt($credentials)) {
            // L'authentification a réussi

            return redirect()->intended('/profil'); // Redirige vers la page de profil après la connexion
        }

        // L'authentification a échoué
        return back()->withErrors(['username' => 'Les informations d\'identification fournies sont incorrectes.']);
    }

// Méthode pour gérer la déconnexion
    public function logout(Request $request)
    {
        Auth::logout(); // Déconnectez l'utilisateur
        // Supprimer les cookies du site
        $response = redirect()->route('home')->with('message','Vous avez été déconnecté avec succès');
        foreach ($response->headers->getCookies() as $cookie) {
            $response->withCookie($cookie->forever()->expired());
        }
        return $response;
    }
}

