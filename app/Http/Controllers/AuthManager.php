<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;




use Illuminate\Support\Facades\Session;



class AuthManager extends Controller
{

    //la méthode pour afficher le formulaire de connexion
      public function login(){
        return view('login');
    }

    // parti du code pour l'enregistrement d'un new utilisateur
  public  function registration(){
        if (Auth::check()){
            return redirect(route('home'));
        }
        return view('registration');
    }

// validation des informations de connexion de l'utilisateur

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function loginPost(Request $request){
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->only('username', 'password');

        if(Auth::attempt($credentials)){
            // Mise à jour de la date de dernière visite
            Auth::user()->update(['last_login' => now()]);

            // Redirigez l'utilisateur vers le profil avec un message de bienvenue
            return redirect()->route('profil')->with('success', 'Bienvenue sur votre profil');
        }

return redirect(route('login'))->with("error", "Les informations de connexion ne sont pas valides");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

// validation des données d'inscription, de la création d'un nouvel utilisateur
    public function registrationPost(Request $request){
        $request->validate([
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);

        // Compter le nombre d'utilisateurs existants
        $userCount = User::count();

// Définir le rôle en fonction du nombre d'utilisateurs
        if ($userCount == 0) {
            $adminRole = 1;
        } else {
            $adminRole = 0;
        }


// Créer les données pour l'utilisateur
        $data = [
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'role' => $adminRole,
        ];


        // Créer l'utilisateur
        $user = User::create($data);
        if(!$user){
            return redirect(route('registration'))->with("error", "Échec de l’enregistrement, réessayer.");
        }
        return redirect(route('login'))->with("success", "L'utilisateur a été créé avec succès");
    }





    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */

    //  la validation des informations de connexion de l'utilisateur

    public function navigation()
    {
        $userRole = Auth::check() ? Auth::user()->role : null;

        return view('navigation', ['userRole' => $userRole]);
    }
    public function show()
    {
        // Utiliser le middleware 'auth' pour s'assurer que seul l'utilisateur authentifié peut accéder à cette page
        $this->middleware('auth');

        // Récupérer l'utilisateur connecté
        $utilisateur = auth()->user();

        // Vérifier si l'utilisateur existe
        if (!$utilisateur) {
            return redirect()->route('home')->with('erreur', 'L\'utilisateur n\'a pas été trouvé');
        }

        // Afficher la vue du profil avec les données de l'utilisateur
        return view('profil', compact('utilisateur'));

    }



}







