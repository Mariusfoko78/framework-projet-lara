<?php
namespace App\Http\Controllers;
use App\Models\Course;


class CourseController extends Controller

{
    public function course ()
    {
       $courses = Course::all();
       return view('course', ['courses' => $courses]);
    }
}

