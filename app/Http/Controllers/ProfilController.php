<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class ProfilController extends Controller
{
    public function show()
    {
        // Utilisez le middleware 'auth' pour s'assurer que seul l'utilisateur authentifié peut accéder à cette page
        $this->middleware('auth');
        // Récupérez l'utilisateur connecté
        $user = auth()->user();
        // Vérifiez si l'utilisateur existe
        if (!$user) {
            return redirect()->route('home')->with('erreur', 'L\'utilisateur n\'a pas été trouvé');
        }
        // Renvoyez les données de l'utilisateur vers la vue du profil
        return view('profil', compact('user'));
    }


    public function edit(User $user)
    {
        // $this->authorize('update', $user); // Assurez-vous que l'utilisateur est autorisé
        return view('edits', compact('user'));
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $request->validate([
            'password' => 'nullable|string|min:8',
            'email' => 'nullable|string|email|max:255|unique:users,email,' . $user->id,
        ]);

        $user->fill([
            'password' => $request->input('password') ? Hash::make($request->input('password')) : $user->password,
            'email' => $request->input('email')?$request->input('email'):$user->email
        ])->save();

        return redirect()->route('profil')->with('success', 'Profil mis à jour avec succès');
    }

    public function export()
    {
        $user = Auth::user(); // Récupérez l'utilisateur actuellement connecté

        if (!$user) {
            return redirect()->route('login'); // Redirigez vers la page de login si l'utilisateur n'est pas connecté.
        }

        $data = [
            'identifiant' => $user->identifiant,
            'email' => $user->email,
            'created_at' => $user->created_at->format('Y-m-d H:i:s'), // Format de date et heure au besoin
            'last_login' => $user->last_login ? $user->last_login->format('Y-m-d H:i:s') : 'N/A',
            'role' => $user->role,
        ];

        // Convertissez les données en format JSON
        $json_data = json_encode($data, JSON_PRETTY_PRINT);

        // Enregistrez le fichier JSON sur le serveur (par exemple, dans le dossier storage/app)
        $file_path = storage_path('app/data_user.json');
        file_put_contents($file_path, $json_data);
        // Créez une réponse de téléchargement avec le fichier JSON
        return response()->download($file_path, 'data_user.json', ['Content-Type' => 'application/json']);
    }
}






