<?php


namespace App\Http\Controllers;
use App\Http\Request\AdminRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Models\User;

class AdminController extends Controller
{
    /**
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function List()
    {
        $users = User::all();
        return view('admin.userList', ['users'=> $users]);
    }


    public function Role(Request $request, $id)
    {
        $user = User::findorfail($id);
        $request->validate([
            'role'=> 'required|in:visiteur,étudiant,professeur',
        ]);

        if($user->isAdmin() && auth()->user()->can('changeRole', $user)) {
            $user->role = $request->input('role');
            $user->save();
            return redirect()->route('admin.users')->with('success', 'Rôle de l\'utilisateur ' . $user->username . ' a été mis à jour avec succès !');
        } else {
            return redirect()->route('admin.users')->withErrors('Impossible de changer le rôle de cet utilisateur.');
        }
    }
 }
